<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
	http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <distributionManagement>
        <repository>
            <uniqueVersion>false</uniqueVersion>
            <id>mcib3d</id>
            <name>mcib3d-core</name>
            <url>file:///home/thomas/ownCloud/PROG/MCIB/3d-suite-imagej/docs/release/</url>
            <layout>default</layout>
        </repository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>utf-8</project.build.sourceEncoding>
    </properties>

    <groupId>org.framagit.mcib3d</groupId>
    <artifactId>mcib3d-core</artifactId>
    <version>4.0.93h</version>
    <packaging>jar</packaging>

    <name>MCIB3D-CORE</name>
    <description>The mcib3d library is the core library for most of the mcib3d plugins.</description>
    <url>https://mcib3d.frama.io/3d-suite-imagej/</url>
    <inceptionYear>2012</inceptionYear>

    <licenses>
        <license>
            <name>GNU General Public License</name>
            <url>https://gnu.org/licenses/gpl.html</url>
            <distribution>repo</distribution>
            <comments>Version 3 of the License or (at your option) any later version.</comments>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Thomas Boudier</name>
            <email>thomas.boudier@sorbonne-universite.fr</email>
            <roles>
                <role>main developer</role>
            </roles>
        </developer>
    </developers>

    <contributors>
        <contributor>
            <name>Ollion Jean</name>
            <roles>
                <role>developer</role>
            </roles>
        </contributor>
    </contributors>

    <dependencies>
        <!-- ImageJ and 3D Viewer -->
        <dependency>
            <groupId>net.imagej</groupId>
            <artifactId>ij</artifactId>
            <version>1.54b</version>
        </dependency>
        <dependency>
            <groupId>sc.fiji</groupId>
            <artifactId>3D_Viewer</artifactId>
            <version>4.0.3</version>
        </dependency>
        <!-- Java 3D dependencies -->
        <dependency>
            <groupId>org.scijava</groupId>
            <artifactId>j3dcore</artifactId>
            <version>1.6.0-scijava-2</version>
        </dependency>
        <!-- Misc. -->
        <dependency>
            <groupId>com.googlecode.combinatoricslib</groupId>
            <artifactId>combinatoricslib</artifactId>
            <version>2.3</version>
        </dependency>
        <dependency>
            <groupId>com.github.quickhull3d</groupId>
            <artifactId>quickhull3d</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>nz.ac.waikato.cms.weka</groupId>
            <artifactId>weka-dev</artifactId>
            <version>3.9.6</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/ome/bio-formats_plugins -->
        <dependency>
            <groupId>ome</groupId>
            <artifactId>bio-formats_plugins</artifactId>
            <version>6.9.1</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>ome</id>
            <url>https://artifacts.openmicroscopy.org/artifactory/ome.releases</url>
        </repository>
        <repository>
            <id>ij</id>
            <url>https://maven.imagej.net/content/repositories/releases/</url>
        </repository>
        <repository>
            <id>scijava</id>
            <url>https://maven.scijava.org/content/repositories/public/</url>
        </repository>
    </repositories>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.4.2</version>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
