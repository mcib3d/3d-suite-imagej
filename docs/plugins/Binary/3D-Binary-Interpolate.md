## 3D Binary Interpolate
This plugin will perform a 3D binary **interpolation** and optionally will create a **isotropic** version of your data (the pixel size is identical in _X_, _Y_, and _Z_).

For binary interpolation, the plugin will look for empty slices with binary mask on _both_ the upper and lower slice, then it will create a intermediate 2D slice there.
This plugin can be used when manual annotation is performed, but not the slices are annotated.
