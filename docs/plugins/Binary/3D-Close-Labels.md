## 3D Close Labels
This plugin will take a labelled image and perform a 3D _closing_ operation on the objects. This will tend to close small holes in the objects and make the shape smoother; it can be used with large radius, equivalent to the objects radius, and it is best used after segmentation. In latest version new operations were added : _dilate_, _erode_ and _fill holes 2D_.

![before closing lables](../../imgs/closeLabels-1.png)

Before closing labels

![after closing lables](../../imgs/closeLabels-2.png)

After closing labels
