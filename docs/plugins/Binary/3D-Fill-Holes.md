## 3D Fill Holes
This plugin will perform a 3D binary fill holes, _i.e_ the hole must be closed in the 3D dimension _X_, _Y_, and _Z_.