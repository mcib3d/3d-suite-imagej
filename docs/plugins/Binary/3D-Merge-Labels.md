## 3D Merge Labels
This plugin will take two labels image and will merge them. If a detection occurs only in one image, it will be kept. If two detections overlap between the two images, the *intersection* or the *union* of the two detections will be computed.

![merge1](../../imgs/merge1.png) ![merge2](../../imgs/merge2.png)

First and second labels image, the *blue* detection is common between the two images.

![merge inter](../../imgs/merge1_merge2_inter.png) ![merge union](../../imgs/merge1_merge2_union.png)

The merged image, with *intersection* on the left and *union* on the right.