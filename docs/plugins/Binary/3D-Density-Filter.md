## 3D Density

This plugin will compute the density of objects in a image based on distance analysis.

The **NbNeighbors** parameter will determine the number of closest neighbours to compute from each pixel. The distances are sum weighted using this formula :

\\(\sum_{\forall i}{e^{-{dist_i}^2*C}}\\)

where \\(dist_i\\) is the distance to the \\(i_{th}\\) closest point, and \\(C={{1} \over{2*\sigma^2}}\\) . 

The parameter **radius** is assimilated to σ and determines roughly the radius of _expansion_ from the spot centre.

![density](../../imgs/density.png)

Density computation for the spots in top left, with increasing radius 5, 10 and 15.
