## 3D Convex Hull

This plugin will compute the convex hull of 3D objects.

The plugin will require one images as input. The input image can be a binary or labelled image, in case of a binary image, a labelling will be performed. This plugin uses the algorithm from [QuickHull3D](http://quickhull3d.github.io/quickhull3d/).

![Hull](../../imgs/hull.png)
