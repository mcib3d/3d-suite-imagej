## 3D Draw ROIs 

This plugin allows you to draw the 3D ROIs from a labelled image on top of the raw image for visual inspection. 

First select the **raw** and labelled (**seg**) image.

![3DRois-1](../../imgs/drawRoi-1.png)
![3DRois-2](../../imgs/drawRoi-2.png)

The default drawing will use ImageJ 2D ROIs on all slices, you can choose to display each individual ROI contour with its own label with **display 3D labelled Roi**.

![3DRois-4](../../imgs/drawRoi-4.png)
![3DRois-3](../../imgs/drawRoi-3.png)




