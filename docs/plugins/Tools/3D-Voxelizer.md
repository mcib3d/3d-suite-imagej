## 3D Voxelizer

This plugin will create a 3D binary mask from a mesh file in a format that can be opened by *3D Viewer*, such as *.stl*.

![Rabbit](../../imgs/rabbit.png)

In green the original *Stanford Bunny* as a mesh, in white the voxelized scaled version, both visualized within *3D Viewer*. 