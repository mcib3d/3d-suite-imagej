## 3D Draw Shape

This plugin allows you to draw 3D shapes (ellipsoids). You can either choose to draw the shape in the current image, create a new one or create the shape in 3D Viewer. 

There are different parameters to set, first, if you want to create a new 3D stack, you must specify the size of the **image** with the width, height and depth separated by commas _,_ like _512,512,512_.

When you create a new stack you must specify the pixel **calibration** by specifying the calibration for _X_ and _Y_ axes, the _Z_ axis, and the _unit_. 

Then you need to specify the **centre** and the different **radii** of the ellipsoid. The values are in unit. The 3 coordinates and 3 radii (in descending order) are separated by commas.

The **orientation** of the ellipsoid is specified by the first two axes corresponding to the first two largest radii, _Vector1_ for the largest radius and _Vector2_ for the second largest radius. The two vectors should be perpendicular. The 3 coordinates  are separated by commas. 

Finally you specify some options for the display, like the **value** the shape should be drawn. If the **gradient** option is checked, a gradient will be created inside the shape with highest value at the centre.

For the **display**, you can choose to create the shape in a _new stack_, or _overwrite_ in the current stack, or no display with _none_. The **3D viewer** option will create a mesh surface of the shape in the 3D Viewer.
