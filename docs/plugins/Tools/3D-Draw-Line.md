## 3D Draw Line

This plugin allows you to draw 3D shapes (ellipsoids). You can either choose to draw the shape in the current image, create a new one or create the shape in 3D Viewer. 

There are different parameters to set, first, if you want to create a new 3D stack, you must specify the sizes in _X_, _Y_ and _Z_ of the **image**.

You then specify the **coordinates**, in pixel unit, of the start and end point of the line. 

The two start and end points can be **marked**, with small sphere, if you select _mark ends_. 

In case an image is opened, you can view the **profile** along the line you specified above by selecting _plot profile_.

You can select the **thickness** of the line, in pixel unit, and its pixel **value**.

For the **display**, you can choose to create the shape in a _new stack_, or _overwrite_ in the current stack, or no display with _none_. The **3D viewer** option will create a mesh surface of the shape in the 3D Viewer. 



