## 3D RDAR

This plugin is a 3D implementation of the algorithm described in _Quantitative morphometric analysis of hepatocellular carcinoma : development of a programmed algorithm and preliminary application_ by Yap _et al._ ([link](https://www.dirjournal.org/en/quantitative-morphometric-analysis-of-hepatocellular-carcinoma-development-of-a-programmed-algorithm-and-preliminary-application-13931))

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

A fitting by a 3D ellipsoid is performed, then the number and volume of the parts of the object popping in or out from the ellipsoid are computed.

![RDAR](../../../imgs/RDAR.png)

In green the parts popping out and in magenta popping in from the ellipsoid in blue (and magenta).

