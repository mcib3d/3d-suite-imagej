## 3D Shape measurements

This plugin will compute measurements about the shape the objects.

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image

Then we have the compactness  values: 

\\(C = \frac{36.\pi.{V_{ol}}^2}{{A_{rea}}^3}\\)

- **Compactness(pix)**: the compactness value computed using volume and surface in pixel unit
- **Compactness(unit)**: the compactness value computed using volume and surface in calibrated unit
- **CompactCorrected(pix)**: the compactness value computed using volume in pixel unit and corrected surface (pixel unit)
- **CompactDiscrete(pix)**: a compactness value for binary objects

The sphericity values are related to the compactness values:


\\(S = C^{\frac{1}{3}}\\)

- **Sphericity(pix)**: the sphericity value computed using volume and surface in pixel unit
- **Sphericity(unit)**: the sphericity value computed using volume and surface in calibrated unit
- **SpherCorrected(pix)**: the sphericity value computed using volume in pixel unit and corrected surface (pixel unit)
- **SpherDiscrete(pix)**: a sphericity value for binary objects


 
 
