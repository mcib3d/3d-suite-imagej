## 3D Ellipsoid measurements

This plugin will compute the different measurements based on the fitting of a 3D ellipsoid. The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The ellipsoid axes and radii are computed using 3D moments : 

- \\( sxx = \sum{{(C_x-x)^2}\over{V_{obj}}} \\)
- \\( syy = \sum{{(C_y-y)^2}\over{V_{obj}}} \\)
- \\( szz = \sum{{(C_z-z)^2}\over{V_{obj}}} \\)
- \\( sxy = \sum{{(C_x-x).(C_y-y)}\over{V_{obj}}} \\)
- \\( sxz = \sum{{(C_x-x).(C_z-z)}\over{V_{obj}}} \\)
- \\( syz = \sum{{(C_y-y).(C_z-z)}\over{V_{obj}}} \\)

The three eigen values and vectors are then computed from this matrix : 

\\(
\begin{pmatrix}
   sxx & sxy & sxz \\\
   sxy & sy2 & syz \\\
   sxz & syz & szz
\end{pmatrix}
\\)

The three radii are then : \\(\sqrt{(5*value)}\\). Review about 3D Moments computation is available [here](https://en.wikipedia.org/wiki/List_of_moments_of_inertia).

![ellipsoid](../../../imgs/ellipsoid.png)

The different values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image  

Then the values based of the ellipsoid volume.

- **VolEll(unit)**: the ellipsoid volume in calibrated unit
- **Spareness**: the ratio between the ellipsoid volume and the object volume

Then values based on the three radii.

- **EllMajRad(unit)**: the length of the major radius of the ellipsoid in calibrated unit
- **Elongation**: the ratio between the length of the major radius to the length of the second radius
- **Flatness**: the ratio between the length of the second radius to the length of the third radius
