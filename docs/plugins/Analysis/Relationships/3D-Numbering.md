## 3D Numbering

With this plugin you can count the number of objects inside other objects, the objects must be defined into two different labelled images.

![Numbering](../../../imgs/numbering.png)

The values in the results table are:

- **Value**: the pixel value of the object in image A
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image of the object in the labelled image
- **Label**: the name of the image (usually referred as _label_ in other plugins)

Then the computed values for each object in image _A_.

- **NbObjects**: the number of objects _B_ in this object in image _A_
- **VolObjects**: the total volume of the objects in image _B_ in this object in image _A_