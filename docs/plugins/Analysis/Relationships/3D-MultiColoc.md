## 3D Multi Co-localisation

With the plugin **3DMultiColoc** and **3DMereoTopology** you can quantify the relationships between objects. 

**MultiColoc** will compute all the colocalisation between all possible pairs of objects between two images. The colocalisation is defined here as the volume of the intersection between two objects. 

![MultiColoc](../../../imgs/multicoloc.png)

In this example left image(A) has 6 objects and right image(B) has 4 objects. The _ColocAll_ results table display all the possible colocalisation between objects in A and objects in B. Objects are labelled with their pixel value in the image. Object A1 correspond to object having value 1 in image A. Same for objects in B, they are labelled with their pixel values. 

The _ColocOnly_ results table display for each object A the list of objects in B colocalised with the object in A, **O** is the object value in B, **V** is the colocalised volume between object A and object B, and **P** is the percentage of object in A colocalised with object in B. 


 
