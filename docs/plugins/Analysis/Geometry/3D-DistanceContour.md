## 3D Distance Centre Contour

This plugin will compute the statistics of the distances from the centre to the contour voxels. If the centre of the object is not inside the object, the plugin will return null values (_NaN_).

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image  

Then the values in pixel unit.

- **DCMin(pix)**: the minimum value of the distances centre-contour in pixel unit
- **DCMax(pix)**: the maximum value of the distances centre-contour in pixel unit
- **DCAvg(pix)**: the average value of the distances centre-contour in pixel unit
- **DCsd(pix)**: the standard deviation value of the distances centre-contour in pixel unit

And the values in calibrated unit.

- **DCMin(unit)**: the minimum value of the distances centre-contour in calibrated unit
- **DCMax(unit)**: the maximum value of the distances centre-contour in calibrated unit
- **DCAvg(unit)**: the average value of the distances centre-contour in calibrated unit
- **DCsd(unit)**: the standard deviation value of the distances centre-contour in calibrated unit

