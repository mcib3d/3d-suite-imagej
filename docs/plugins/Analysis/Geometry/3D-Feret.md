## 3D Feret

This plugin will compute the 3D Feret diameter, along with the two points being farthest apart in the object. Since the distance between all pairs are computed, the computation of the Feret value can take a while.

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed. 

The different values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image  

Then the feret diameter value in calibrated unit.

- **Feret(unit)**: the Feret diameter in calibrated unit

Then the first voxel of the diameter, in pixel unit.

- **Feret1X(pix)**: the _X_ coordinate of the first voxel of the Feret diameter
- **Feret1Y(pix)**: the _Y_ coordinate of the first voxel of the Feret diameter
- **Feret1Z(pix)**: the _Z_ coordinate of the first voxel of the Feret diameter

And the second voxel of the diameter, in pixel unit.

- **Feret2X(pix)**: the _X_ coordinate of the second voxel of the Feret diameter
- **Feret2Y(pix)**: the _Y_ coordinate of the second voxel of the Feret diameter
- **Feret2Z(pix)**: the _Z_ coordinate of the second voxel of the Feret diameter

