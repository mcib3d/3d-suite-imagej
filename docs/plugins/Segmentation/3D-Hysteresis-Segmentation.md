## 3D Hysteresis Segmentation
This plugin will perform 3-levels thresholding based on two thresholds. The _low_ threshold will determine background (for values below), the _high_ threshold the objects cores (for values above) and an intermediate level for values between low and high threshold. 

![3D Hysteresis 1](../../imgs/hyst-1.png)

Original image

![3D Hysteresis 2](../../imgs/hyst-2.png)

Image thresholded with low and high threshold, determining three levels (background is black, object cores is white, intermediate is gray). 

The hysteresis algorithm will convert intermediate pixels connected to core pixel to core pixels, intermediate pixels not connected to core will become background. 

![3D Hysteresis 3](../../imgs/hyst-3.png)

Image thresholded with hysteresis algorithm

The plugin can also directly label the thresholded image.

![3D Hysteresis 4](../../imgs/hyst-4.png)

Labelled image, only objects with brighter core are detected

