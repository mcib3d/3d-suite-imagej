## 3D Spot Segmentation

The plugin works with two images, one containing the **seeds** of the objects, that can be obtained from local maxima (see _3D Filters_ or _3D Maxima Finder_), the other image containing signal data. The program computes a **local threshold** around each seeds and cluster voxels with values higher than the local threshold computed.  

Three methods are available for computing the value of the local threshold, and 3 methods for clustering are also proposed. The option **watershed** can be chosen to avoid merging of close spots.A tutorial is also avalaible : [3D Spot Segmentation Manual](../../../uploads/3d_seg_spot_tutorial.pdf).

![spot-seg-1](../../../imgs/heck-orig.png)

Original image with Fire LUT, seeds are computed as local maxima

![spot-seg-2](../../../imgs/heck-watershed.png)

Watershed zones segmentation of the seeds

![spot-seg-3](../../../imgs/heck-seg.png)

Final segmentation of the spots

