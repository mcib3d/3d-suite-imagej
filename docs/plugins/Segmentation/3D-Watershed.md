## 3D Watershed (segmentation, split, and zones)

The **3D Watershed** plugin works with two images, one containing the seeds of the objects, that can be obtained from local maxima (see 3D filters), the other image containing signal data. A first _threshold1_ is used for seeds (only seeds with _value > threshold1_ will be used). A second threshold is used to cluster voxels with _values > threshold2_. In this implementation voxels are clustered to seeds in descending order of voxel values.

## 3D segmentation
The seeded version implemented in this plugin will aggregate voxels with higher values first to the seeds. Two seeds with different values for neighbouring voxels may not be growing at same speed, the one with higher values will grow faster than the one will lower values.

![3d-wat-1](../../imgs/wat1.png)
![3d-wat-2](../../imgs/wat2.png)

Original test image with overlaid seeds, and final result of watershed segmentation

## Classical segmentation

The classical segmentation with watershed is based on the  gradient (_Find Edges_) of the image. First we can find the seeds using local extrema. In this implementation we need to _invert_ the edge image. Then we apply the watershed on the edge image starting from the seeds. 

![3d-watc-1](../../imgs/gel-1.png)
![3d-watc-2](../../imgs/gel-2.png)

Original image and inverted edge image

![3d-watc-3](../../imgs/gel-3.png)
![3d-watc-4](../../imgs/gel-4.png)

Seeds as local minima and final watershed result

## 3D Watershed splitting
The main application of _watershed_ in ImageJ is the 2D splitting of merged objects.

This splitting is based on the computation of the _Distance Map_ inside the mask of the merged objects. The seeds are then the local maxima of the distance map, the farthest points from the boundaries, hence corresponding to the centres of the objects. The bigger the object, the higher the values of the distance map at the centre, then the faster the growing of the seeds and the larger the resulting object.

![3d-split-1](../../imgs/wat1split.png)
![3d-split-2](../../imgs/wat2split.png)
![3d-split-3](../../imgs/wat3split.png)

## Voronoi zones
The Voronoi algorithm will draw lines between objects at equal distances from the boundaries of the different objects, then compute zones around objects based on these lines. This can also be seen as the splitting of the background.

Neighbouring objects can then be computed as objects having a line in common. 

![3d-zones-1](../../imgs/voro1.png)
![3d-zones-2](../../imgs/voro2.png)
![3d-zones-3](../../imgs/voro3.png)

