## 3D Manager macro programming

The 3D Manager can be used for batch processing alongside with the various independant plugins for quantification.

The list of available macro functions are available [here](../../uploads/MacrosFunctionsRoiManager3D.pdf).

Here a list of example macros :


- [Measurements](../../code/macromanager3d2_measure.ijm)

- [Quantification](../../code/macromanager3d_quantifmultichannel.ijm)

- [Quantification with multiple colums](../../code/macromanager3d_quantifmultichannel2.ijm)

- [List selected objects](../../code/macromanager3d2_selected.ijm)

- [Draw spheres at centres position](../../code/macromanager3d2_centres.ijm)

- [Find smallest and biggest object](../../code/macromanager3d2_minmax.ijm)

- [Create a colour gradient based on volumes](../../code/macromanager3d2_colorsize.ijm)

- [View objects in 3D viewer with random colours](../../code/macromanager3d2_colorsviewer3d.ijm)

- [View objects in 3D viewer with LUT colours](../../code/macromanager3d2_lutviewer3d.ijm)

- [Compute and display clusters](../../code/macromanager3d2_cluster.ijm)


