## 3D Fast Filters

3D filtering using ellipsoidal neighbourhood, as part of the [3D ImageJ Suite](/3d-suite-imagej/).

## Author

Thomas Boudier

Matthias Labschütz contributed a fast version for isotropic kernels (for min, max and mean filtering)

## Description

The plugin **3D Fast Filters** performs various filtering on image stacks. The kernel is ellipsoidal, you can then select three radii for _X_, _Y_, and _Z_. Note that the plugin uses parallel implementation, and a fast version using _rolling ball_ algorithm is used for isotropic kernel (all three radii equal).

The  filters  mean, median, variance, min and max are implemented in ImageJ since version 1.47.

Available filters :

- Mean and variance filtering

- Minimum and maximum filtering (erosion and dilation for gray-level images)

- Open and close

- Local maxima

- TopHat transform (to detect spots-like objects)

- Sobel (simple edge detection)

- Adaptive Nagao-like

![3d-variance](../../imgs/3d-variance.png)

3D variance

## Download

For details go to [3D ImageJ Suite](/3d-suite-imagej/).

If you want faster version using GPU, please refer to [CLIJ](https://clij.github.io/).

A [Google Colab notebook](https://colab.research.google.com/drive/1-Y4g7uvLKC8PiSM3Y0qpEGC2YreH0k8s?usp=sharing) is availabe to test the plugin without any installation and check the code.

## Citation

When using the *3D Filters* plugin for publication, please refer to :

J. Ollion, J. Cochennec, F. Loll, C. Escudé, T. Boudier. (**2013**) TANGO: A Generic Tool for High-throughput 3D Image Analysis for Studying Nuclear Organization. *Bioinformatics* 2013 Jul 15;29(14):1840-1. [doi](http://dx.doi.org/10.1093/bioinformatics/btt276)

## License

GPL distribution (see [license](http://www.cecill.info/index.en.html)).

## Changelog

- 10/04/2017 : new timer for computing time
- 28/05/2013 : new filters Open and Close
- 22/10/2012 : bug fixed for filters3D with 32-bits images
- 22/06/2011 : Parallelization updated

## Known Bugs

- ij1.47d : Bug in Color/Split, the resulting stacks have incorrect bitdepth
