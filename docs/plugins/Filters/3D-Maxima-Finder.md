## 3D Maxima Finder tutorial  
Using same idea as the [ImageJ Find Maxima](http://rsb.info.nih.gov/ij/docs/guide/146-29.html#toc-Subsection-29.4) function, the 3D Maxima Finder detects local maxima with high contrast. The 3D Maxima Finder plugin is part of the [3D ImageJ Suite](/3d-suite-imagej/). 

## Basic principle
Local maxima in a specified radius are computed and sorted by intensity. Starting from local maximum with highest intensity, a 3D flooding is performed, all connected pixels with values above the value of (*LocalMaxima - NoiseValue*) are marked as zone of the local maximum. All other local maxima from these zones are removed. The next local maximum is processed, this step is repeated until there are no more local maxima to process. The final image is created with local maxima, with their original intensity value.

![3DMaximaFinder1](../../imgs/3d-maxima-finder-1.png)

Original image

![3DMaximaFinder2](../../imgs/3d-maxima-finder-2.png)

Raw local maxima

![3DMaximaFinder3](../../imgs/3d-maxima-finder-3.png)

Local zones around local maxima

![3DMaximaFinder4](../../imgs/3d-maxima-finder-4.png)

Maxima found

## Application
The main application of this method is  objects detection (such as spots) and numbering. The local maxima detected can be used as seeds for the [3D Spots Segmentation plugin](https://imagej.net/plugins/3d-segmentation#3d-spot-segmentation).


## Code sample
A [Google Colab notebook](https://colab.research.google.com/drive/1tCFZ7O9n6zephuutQb2miSK6juvFkm4m?usp=sharing) is availabe to test the plugin without any installation and check the code.
