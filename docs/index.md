## 3D ImageJ Suite

This *suite* provides plugins to enhance 3D capabilities of ImageJ.

Updated contents from [3D Suite on ImageJ Documentation Website](https://imagejdocu.list.lu/plugin/stacks/3d_ij_suite/start).

The pdf of the [NEUBIAS academy](http://eubias.org/NEUBIAS/training-schools/neubias-academy-home/) presentation "*Introduction to 3D Analysis with 3D ImageJ suite*" is available : ([pdf](./uploads/neubias-3dsuite.pdf)).

The associated video is available on [YouTube](https://www.youtube.com/watch?v=OPC2kP-5By4). The QAs are available on the [image.sc forum](https://forum.image.sc/t/neubias-academy-home-webinar-introduction-to-3d-analysis-with-3d-imagej-suite-questions-answers/39027).

The associated project for automation is available in [TAPAS](https://mcib3d.frama.io/tapas-doc/).

## Author

Thomas Boudier, [Centrale Méditerranée](https://www.centrale-mediterranee.fr/en), Nice Campus, France.

You can contact me on [image.sc](https://forum.image.sc/) **@ThomasBoudier**.

With many contributions from J. Ollion, Laboratoire Jean Perrin, Paris, France.

## Features

This *suite* is composed of plugins to perform different tasks on image stacks :

- **Filters** for pre- and post-filtering. Filters can be used to reduce noise (see median filter in _3D Fast Filters_). Some filters (edge and _symmetry_ or _maxima finder_) can help detect seeds for further segmentation (such as _spot segmentation_ or _watershed_)

- **Binary** filters will improve thresholding or segmentation results such as _fill holes_ or _exclude objects on edges_. For labelled image _close labels_ can help make objects shape more compact. Maps can also be created such as _distance map_ to compute distance from all pixels to the border of the labelled objects. 

- **Segmentation** for objects detection. Thresholding will create a binary image and can be performed using _Adjust/Threshold_. Labelling will detect the different objects in a binary image (see _simple_, _hysteresis_ or _iterative_ segmentation plugins). For seeds-based detection _watershed_ segmentation can be used. Custom segmentation plugins are also available for _spots_ or _nuclei_ (in culture cells images).

- **Analysis** to perform various 3D measurements of the objects, such as _geometry_, _shape_, or -intensity-. _Relationships_ between objects can also be studied with plugins such as _distances_ or _co-localisation_. Finally _3D spatial organisation_ can be assessed with plugins such as _EVF Analysis_ or _Spatial Statistics_.

- The **3D Manager** interface acts a a manager for 3D objects, to perform measurements.

- Various **tools** are also available, especially to create _3D Shapes_ or _Draw 3D contours_ as Rois. 

<iframe style="width:800px;height:600px;border: 1px
solid black" src="https://framindmap.org/c/maps/1128494/embed?zoom=1"> </iframe>

## Installation

From version 2.7, **3D ImageJ Suite** is available in Fiji as an [update site](https://imagej.net/list-of-update-sites/). The **Java8** update site must be activated in Fiji. For versions 2 and 3, the **ImageScience** update site must also be activated in Fiji.

You can also install **3D ImageJ Suite** manually (see download section) and unzip it in your plugins folder. The various plugins will appear in the menu **3D** of the plugins list.

For versions 2 and 3, you have also to manually download and copy into your plugins directory the **imagescience.jar** library from [here](http://www.imagescience.org/meijering/software/featurej/).

## Download

ImageJ 1.47 or later is required. Java3D is also required, check that [3D Viewer](https://imagej.net/plugins/3d-viewer/index) is working.

From version 3.83 and later, for ImageJ users the new version 4.0.1 of 3D Viewer is required (please remove the 3D Viewer plugin provided by ImageJ and replace it by [this version](./uploads/3d_viewer-mcib.zip), just unzip the file in the plugins directory).

Java 1.8 or later is required. For versions 2 and 3, the [imagescience](http://www.imagescience.org/meijering/software/featurej/) library is required : [download here](http://www.imagescience.org/meijering/software/download/imagescience.jar).

- **Download Bundle** : [mcib3d-suite-4.1.7.zip](./uploads/mcib3d-suite-4.1.7.zip)

- **Download core** : [mcib3d-core-4.1.7.jar](./uploads/mcib3d-core-4.1.7.jar)

- **Download plugins** : [mcib3d_plugins-4.1.7.jar](./uploads/mcib3d_plugins-4.1.7.jar)

Sources are available on [Framagit](https://framagit.org/) for [core](https://framagit.org/mcib3d/mcib3d-core) and [plugins](https://framagit.org/mcib3d/mcib3d-plugins).

## Citation

If you use the 3D suite for your publication, please cite :

J. Ollion, J. Cochennec, F. Loll, C. Escudé, T. Boudier. (**2013**) TANGO: A Generic Tool for High-throughput 3D Image Analysis for Studying Nuclear Organization. *Bioinformatics* 2013 Jul 15;29(14):1840-1. [doi](http://dx.doi.org/10.1093/bioinformatics/btt276)

## License

GPL distribution [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). Sources available on Framagit (see Download).

## Acknowledgements

The 3D suite would like to thank P. Andrey, J.-F. Gilles and the developers of the following plugins :

- [Imagescience](http://www.imagescience.org/meijering/software/featurej/)
- [LocalThickness](https://www.optinav.info/Local_Thickness.htm)
- [ConvexHull3D](https://imagej.nih.gov/ij/plugins/3d-convex-hull/index.html)
- [3D Object Counter](https://imagejdocu.list.lu/plugin/analysis/3d_object_counter/start)
- [Droplet Counter](https://imagejdocu.list.lu/plugin/analysis/droplet_counter/start)

## Links

- [BoneJ](https://imagej.net/plugins/bonej)
- [3D Shapes](https://sites.google.com/site/learnimagej/plugins/3d-shape)
- [LabKit](https://imagej.net/plugins/labkit)
- [3D Viewer](https://imagej.net/plugins/3d-viewer/index)
- [MorphoLibJ](https://imagej.net/plugins/morpholibj)
- [CLIJ](https://clij.github.io/clij-docs/)

## Change Log
- 02/08/2024 V4.1.7: Bug fixes, Generic Thresholder
- 19/04/2023 V4.1.5: Bug fixes, seeds option for segmentation.
- 20/03/2023 V4.1.2: Bug fixes. Intensity Mode Non Zero. Plugin Voxelizer.
- 26/01/2023 V4.1.0: New design for 3DManager. New Macro Extensions. New plugin Merge Labels.
- 23/09/2022 V4.0.93: New Center-Border distances. Improved 3D Maxima Finder. Testing new 3D Manager.
- 30/06/2022 V4.0.88: Minor changes.
- 08/04/2022 V4.0.87: Improved Measurements. New method FillHoles in BinaryCloseLabels.
- 09/02/2022 V4.01: Changed *Value* to *Label* for segmented objects. Corrected bugs in Analysis.
- 05/01/2022 V4.01: Redesigned code for 3D Objects and measurements; new plugins and organisation.
- 30/10/2019 V3.96: Bug fixes and improvements. New plugins distances and interactions.
- 15/01/2019 V3.94: Bug fixes and improvements. 3D Manager interface improved.
- 19/10/2018 V3.93: Bug fixes and improvements. New plugins MultiColoc, EVF radial analysis and density.
- 14/03/2018 V3.92: Bug fixes and improvements. New Roi3D overlay in 3DManager.
- 05/01/2018 V3.91: Bug fixes and improvements.
- 10/04/2017 V3.9 : Display computing time and ETC for filters, new LooknFeel for Manager.
- 22/08/2016 V3.83: New plugin Radial Distance Area Ratio (RDAR), Watershed improved.
- 04/07/2016 V3.82: compatibility with new version of 3D viewer, improvements and bug fixes.
- 30/03/2016 V3.74: bug fixed in EDT.
- 18/03/2016 V3.71: new plugin 3D Maxima Finder.
- 02/03/2016 V3.7 : bug fixes, new plugin exclude objects on borders.
- 07/12/2015 V3.6 : bug fixes, new edge and symmetry filters, sync between 3D Viewer select object and 3DManager.
- 01/09/2015 V3.5 : compatibility with ImageScience 3.0.
- 20/08/2015 V3.4 : redesigned Watershed, Record Iterative Thresholding, Quantification in 32-bits images with NaN values.
- 05/06/2015 V3.3 : new function closeResults saveResults 3DManager + exclude on edges.
- 25/05/2015 V3.2 : improved watershed (especially for flat regions).
- 27/04/2015 V3.1 : bug in colocalisation (rare objects configuration) + improved split + poles in ellipsoid fitting.
- 21/11/2014 V3.0 : closest object in selection for 3D Manager. Macros functions to save results for 3D Manager.
- 03/10/2014 V2.9 : new Results Tables for 3DManager.
- 03/06/2014 V2.8 : new plugin shape analysis. Load/save objects with calibration.
- 21/03/2014 V2.71: bug fix in Segment3D.
- 24/10/2013 V2.7 : new segmentation Iterative thresholding; check for required libraries at startup; 32-bits segmentation and labelling.
- 28/05/2013 V2.6 : bug fixes and improvements, new filters openGray and closeGray.
- 21/03/2013 V2.5 : new design for 3D Manager.
- 08/02/2013 V2.4 : new plugin MereoTopology 3D.
- 29/10/2012 V2.3 : new macro functions for 3D Manager.
- 22/10/2012 V2.3 : bug fixed for filters3D with 32-bits images.
- 12/10/2012 V2.2 : calibration bug fixed in spatial statistics.
- 28/09/2012 V2.1 : corrected bug in display (Ellipsoid3D and RoiManager3D).
- 25/09/2012 V2.0 : first version.
